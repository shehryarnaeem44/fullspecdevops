const chai = require('chai');
const request = require('supertest');
const server = require('../server');
const should = chai.should();


describe('server test suite',()=>{
    it("should get hello world",(done)=>{
        request(server)
        .get("/api/hello")
        .expect(200)
        .end((err,res) => {
            if(err) done(err)
            res.body.express.should.equal('Hello world');
            done();
        })
    })

    it("should post message",(done)=>{
        const message = "test"
        request(server)
        .post("/api/world")
        .send({post:message})
        .set('Accept', 'application/json')
        .expect(200)
        .end((err,res)=>{
            if(err) done(err);
            done();
        })
    })
})