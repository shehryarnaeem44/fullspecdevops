FROM node:12
RUN npm install -g nodemon

WORKDIR /app

COPY . /app
RUN npm install
WORKDIR /app/client
RUN npm install
WORKDIR /app
                                                                               
CMD yarn dev

EXPOSE 5000
EXPOSE 3000